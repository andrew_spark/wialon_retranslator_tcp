import datetime
import pytz
import os
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'wialon_retranslator.settings')
django.setup()


from api.models import GPSBlock, GPSPosInfo, GPSPacket


def write_message_to_db(packet):
    """
    write data from packet to database
    :param packet: dictionary
    :return: no_return
    """

    gpspacket = GPSPacket.objects.create(
        time=datetime.datetime.fromtimestamp(packet['utc_time'], tz=pytz.UTC),
        flags=packet['flags'],
        controller_id=packet['controller_id']
    )
    gpspacket.save()
    gpsposinfo = GPSPosInfo.objects.create(
        longitude=packet['posinfo']['lon'],
        latitude=packet['posinfo']['lat'],
        altitude=packet['posinfo']['alt'],
        course=packet['posinfo']['course'],
        speed=packet['posinfo']['speed'],
        satellites=packet['posinfo']['satellites'],
        packet=gpspacket
    )
    gpsposinfo.save()

    for block in packet['blocks']:
        gpsblock = GPSBlock.objects.create(
            name=block['name'],
            data_type=block['data_type'],
            value=block['value'],
            visibility=bool(block['visibility']),
            packet=gpspacket
        )
        gpsblock.save()

