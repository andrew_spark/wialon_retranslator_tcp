from tornado.ioloop import IOLoop
from tornado.tcpserver import TCPServer
from tornado.iostream import StreamClosedError
import struct
from .writedb import write_message_to_db
from .parser import parse, parser
from wialon_retranslator.settings import TCP_ADDRESS, TCP_PORT


class Server(TCPServer):

    async def handle_stream(self, stream, address):
        while True:
            try:
                print(address)
                # get data size
                data_len_bytes = await stream.read_bytes(struct.calcsize('<I'))
                data_len = parse('<I', data_len_bytes)
                # get message bytes
                message = await stream.read_bytes(data_len)
                gps_packet = parser(message)

                write_message_to_db(gps_packet)
                await  stream.write(struct.pack('b', 0x11))
            except StreamClosedError:
                break


def server_run():
    server = Server()
    server.listen(port=TCP_PORT, address=TCP_ADDRESS)
    IOLoop.current().start()


if __name__ == '__main__':
    server_run()

