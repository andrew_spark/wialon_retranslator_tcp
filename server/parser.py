import struct

def parse(fmt, binary, offset=0):
    """
    Unpack the string

    fmt      @see https://docs.python.org/3/library/struct.html#format-strings
    value    value to be formated
    offset   offset in bytes from begining
    """
    parsed = struct.unpack_from(fmt, binary, offset)
    return parsed[0] if len(parsed) == 1 else parsed

def parser(packet):
    """
     this parser works for little endian architecture only
    :packet (binary struct):
    : dict of values:
    """

    message = {
        'controller_id': None,
        'utc_time': None,
        'flags': None,
        'blocks': [],
        'posinfo': {}
    }

    controlled_id_len = packet.find(0x0)
    # get id, time and flags from message
    (
        message['controller_id'],
        message['utc_time'],
        message['flags']
    ) = parse('> %ds  x i i' % controlled_id_len, packet)
    message['controller_id'] = message['controller_id'].decode('utf-8')
    offset = struct.calcsize('> %ds  x i i' % controlled_id_len)

    data_blocks = packet[offset:]

    while len(data_blocks):
        # construct block info
        block = {
            'type': None,
            'length': 0,
            'data_type': None,
            'visibility': None,
            'name': None,
            'value': None
        }
        offset = struct.calcsize('> h i')
        (block['type'], block['length']) = parse('> h i ', data_blocks)
        data_blocks = data_blocks[offset:]

        # processing any block data
        data_block = parse('> %ds' % block['length'], data_blocks)

        # calculate offset for 2 bytes data
        offset = struct.calcsize('> b b')
        (block['visibility'], block['data_type']) = parse('> b b', data_block)
        # delete 2 bytes data
        data_block = data_block[offset:]

        # calculate size of name
        name_size = data_block.find(0x0)

        # get block name
        block['name'] = parse('> %ds' % name_size, data_block).decode("utf-8")
        offset = struct.calcsize('> %ds b' % name_size)
        data_block = data_block[offset:]

        if block['name'] == 'posinfo':
            block['value'] = {
                'lon': None,
                'lat': 0,
                'alt': 0,
                'speed': 0,
                'course': 0,
                'satellites': 0

            }

            (
                block['value']['lon'],
                block['value']['lat'],
                block['value']['alt'],
            ) = parse('d d d', data_block)
            (
                block['value']['speed'],
                block['value']['course'],
                block['value']['satellites']
            ) = parse('> h h b', data_block, offset=struct.calcsize('d d d'))

        else:
            block['value'] = None
            # text
            if block['data_type'] == 0x1:
                block['value'] = parse('>%ds' % len(data_block), data_block).decode('utf-8')
            # binary value
            elif block['data_type'] == 0x2:
                block['value'] = str(parse('> %db' % len(data_block), data_block))
            # integer value
            elif block['data_type'] == 0x3:
                block['value'] = str(parse('> i', data_block))
            # float value
            elif block['data_type'] == 0x4:
                block['value'] = str(parse('d', data_block))
            # long int value
            elif block['data_type'] == 0x5:
                block['value'] = str(parse('> q', data_block))
            else:
            # if image we don't parse
                pass

        # delete data from data_blocks
        data_blocks = data_blocks[block['length']:]

        if block['name'] == 'posinfo':
            message['posinfo'] = block['value']

        else:
            block.pop('type')
            block.pop('length')
            message['blocks'].append(block)
    return message