from api.models import GPSPacket, GPSPosInfo, GPSBlock
from rest_framework import serializers


class GPSPosInfoSerializer(serializers.ModelSerializer):
    """
    GPS position block serializer
    """
    class Meta:
        model = GPSPosInfo
        exclude = ('packet', 'id')


class GPSBlockSerializer(serializers.ModelSerializer):
    """
    GPS special block serializer
    """
    class Meta:
        model = GPSBlock
        exclude = ('packet', 'id')


class GPSPacketSerializer(serializers.ModelSerializer):
    """
    GPS packet serializer
    (included position block and special blocks)
    """

    position = GPSPosInfoSerializer(source='gpsposinfo', many=False)
    blocks = GPSBlockSerializer(source='packet_block', many=True)

    class Meta:
        model = GPSPacket
        exclude = ('id',)




class GPSControllerSerializer(GPSPacketSerializer):

    class Meta:
        model = GPSPacket
        fields = ('controller_id', )


class GPSTrackReportSerializer(serializers.ModelSerializer):
    """
    GPS Track serializer
    """
    datefrom = serializers.DateTimeField(required=True)
    dateto = serializers.DateTimeField(required=True)

    class Meta:
        model = GPSPacket
        fields = ('controller_id', 'datefrom', 'dateto')