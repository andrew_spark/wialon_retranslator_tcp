from api.serializers import GPSPacketSerializer, GPSTrackReportSerializer,GPSControllerSerializer
from rest_framework.generics import ListAPIView, CreateAPIView,ListCreateAPIView
from rest_framework.response import Response
from api.models import GPSPacket
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import render


def index(request):
    """
    The home page. This renders the container for the single-page app.
    """
    return render(request, 'index.html')

class ControllersListView(ListAPIView):
    """
    Show all available controllers ( gps-trackers) in database
    allowed methods (GET, HEAD, OPTIONS)
    """
    permission_classes = (IsAuthenticated,)
    queryset = [controller['controller_id'] for controller in GPSPacket.objects.values('controller_id').distinct()]

    def list(self, request, *args, **kwargs):
        return Response(self.get_queryset())


class GPSPacketListView(ListAPIView):
    """
    Show all packets in database for all controllers (or selected if in url added parameter controller_id=.) \n
    Allowed methods (GET, HEAD, OPTIONS)
    """
    permission_classes = (IsAuthenticated,)
    queryset = GPSPacket.objects.all()
    serializer_class = GPSPacketSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('controller_id',)


class GPSPacketControllerGetLastView(CreateAPIView):
    """
        Search and return last packet for chosen controller
        input parameter: controller_id
        allowed methods (POST, HEAD, OPTIONS)
    """
    serializer_class = GPSControllerSerializer
    queryset = GPSPacket.objects.all()
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            last_packet = GPSPacket.objects.last_receive_packet(control_id=request.data.get('controller_id'))
            packet_serializer = GPSPacketSerializer(last_packet)
            return Response(packet_serializer.data)
        except GPSPacket.DoesNotExist:
            raise ValidationError('Object does not exist')


class GPSTrackReportView(CreateAPIView):
    """
    GPS Track report view provide api for selecting
    track by date
    Allowed methods (POST, HEAD, OPTIONS)
    input parameters: controller_id,
    datefrom, dateto
    """
    serializer_class = GPSTrackReportSerializer
    queryset = GPSPacket.objects.all()
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            queryset = GPSPacket.objects.filter(
                controller_id=request.data.get('controller_id'),
                receive_time__range=[request.data.get('datefrom'), request.data.get('dateto')])
            packets = GPSPacketSerializer(queryset, many=True)
            if len(queryset) == 0:
                raise ValidationError('Objects does not exist')
            return Response(packets.data)
        except GPSPacket.DoesNotExist:
            raise ValidationError('Objects does not exist')



