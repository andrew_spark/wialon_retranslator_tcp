from django.conf.urls import url, include
from api import views
from rest_framework_jwt.views import refresh_jwt_token, verify_jwt_token
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    url(r'^controllers/', views.ControllersListView.as_view(), name='controllers-list'),
    url(r'^packets$', views.GPSPacketListView.as_view(), name='packets-list'),
    url(r'^last_receive_packet$', views.GPSPacketControllerGetLastView.as_view(), name='last-packet'),
    url(r'^track$', views.GPSTrackReportView.as_view()),
    url(r'^token/update$', refresh_jwt_token),
    url(r'^token/verify$', verify_jwt_token),
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^docs/', include_docs_urls(title='GPS Controller api')),
    url(r'^auth/register/', include('rest_auth.registration.urls')),
]
