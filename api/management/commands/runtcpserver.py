from django.core.management.base import BaseCommand
from server.server import server_run as tcp_server


class Command(BaseCommand):
    """
    Custom manage.py command for running tcp server
    This command don't have parameters.
    All configurations save in settings.py file
    """
    help = 'Run tcp server'

    def handle(self, *args, **options):
        print('TCP server works')
        tcp_server()
