from django.contrib import admin

from .models import GPSBlock, GPSPosInfo, GPSPacket

class BlocksInline(admin.StackedInline):
    model = GPSBlock
    extra = 0


class PosInfoInline(admin.StackedInline):
    model = GPSPosInfo
    extra = 0


class GPSPacketAdmin(admin.ModelAdmin):
    fields = ['controller_id', 'time', 'flags']
    list_display = ['controller_id', 'id', 'receive_time', 'time', 'flags']
    inlines = [PosInfoInline, BlocksInline]
    list_filter = ['receive_time', 'id']
    ordering = ['id']


admin.site.register(GPSPacket, GPSPacketAdmin)

