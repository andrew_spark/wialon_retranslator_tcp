from django.db import models


class GPSPacketManager(models.Manager):
    def last_receive_packet(self, control_id):
        return self.filter(controller_id=control_id).filter().latest('-receive_time')


class GPSPacket(models.Model):
    flags = models.IntegerField()
    controller_id = models.CharField(max_length=256)
    time = models.DateTimeField()
    receive_time = models.DateTimeField(auto_now=True)
    objects = GPSPacketManager()

    def __str__(self):
        return str(self.controller_id+'j: '+str(self.gpsposinfo.latitude))


class GPSPosInfo(models.Model):
    """
    class GPSPosInfo (objects constains position info about gps-unit)
    """
    longitude = models.FloatField()
    latitude = models.FloatField()
    altitude = models.FloatField()
    course = models.IntegerField()
    speed = models.IntegerField()
    satellites = models.IntegerField()
    packet = models.OneToOneField(GPSPacket, on_delete=models.CASCADE)

    def __str__(self):
        return 'posinfo: '+str(self.pk)


class GPSBlock(models.Model):

    """
    class GPSBlock represent data blocks from tcp-packet
    """
    name = models.CharField(max_length=256)
    data_type = models.IntegerField()
    value = models.CharField(max_length=64)
    visibility = models.BooleanField(default=True)
    packet = models.ForeignKey(GPSPacket, on_delete=models.CASCADE, related_name='packet_block')

    def __str__(self):
        return str(self.name)

    class Meta:
        ordering = ['-name']

